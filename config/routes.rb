# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :a_retailer_basket_items_yesterday, only: %i[index]

      resources :basket_items, only: %i[index show] do
        member do
          get :set_invalid
          get :set_approved
          get :set_shop_id
        end
      end
      resources :baskets, only: %i[index show]
      resources :products, only: %i[index show]
      resources :retailers, only: %i[index show]

      resources :z_epharmacies, only: %i[index new edit]
      resources :z_sku_in_shops, only: %i[index]
    end
  end

  # get '*path' => 'about#index'
  root 'about#index'
end
