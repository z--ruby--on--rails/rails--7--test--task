# Ruby on Rails test task

I trimmed down the original text to keep confidential info safe.

In short, though...

There is a pregiven DB dump to be used as a foundation for a DB schema; default tables represent retailers (~shops), baskets (~processed customer's receipt with list on items purchased), basket items (instances of purchased products) and products.

All of the retailers except a special one dump their revenue data (~purchase history) to our DB. The special one (~marketplace) aggregates several retailers not all of which interest us. _The point of the task is to identify and invalidate basket item data coming from irrelevant shops._

An API endpoint somewhere on the net returning the list of products by SKU identifier is parsed by the service (executed by scheduled rake task every night) I wrote to filter out "invalid" data reading the whitelist of valid shops (`z_epharmacies`).


## Documentation

This application is build upon Ruby on Rails framework.
Technology stack and services:

[!] All three services' images are Alpine Linux based.

1. API - Ruby on Rails

  Partially conforms to RESTful principle but sometimes uses Rails' tricks which are not too traditional (every request is GET, there are no POST, PUT, DELETE, UPDATE etc.). Endpoints are listed at the help dashboard (root url).

2. DB - PostgresQL
```
  # main tables from the task description (created via dump restore)
  basket_items
  baskets
  products
  retailers

  # accessory tables (created with rails migrations)
  z_epharmacies
  z_sku_in_shops
```

3. Monitor - shell

  Created as a status notifier that I used for debugging
  (to detect when service goes down or switches state).

### Requirements
* bash
* postgresql-libs (Postgres client drivers)
* podman+podman-compose (or docker+docker-compose) + `unqualified-search-registries = ["docker.io"]`

### .env file variables
```
  variable            # meaning
  ____________________#_________________________________________________
  APP_DB_BASENAME     # project name and base for DB names
  APP_DB_PASSWORD     # password for project's DB
  APP_DB_USERNAME     # project's DB username
  APP_HOST            # hostname used for link generation in serialisers
  APP_PORT            # port where Rails' Puma server listens
  APP_PROTO           # protocol for API's urls
  BUNDLER_VERSION     # version of Bundler gem used
  CONTAINER_COMPOSER  # podman-compose or docker-compose
  CONTAINER_ENGINE    # podman or docker
  POSTGRES_PASSWORD   # password of system's DB user
  POSTGRES_USER       # username of system's DB user
  RAILS_VERSION       # version of Rails gem used
  RUBY_VERSION        # version of Ruby used
  TZ                  # timezone to be used for all services
  Z_GID               # local user's group id
  Z_GROUP             # local user's group name
  Z_UID               # local user's id
  Z_USER              # local user's name
```
[!] I did not try rootless podman/podman-compose but they probably will work.

### Setup (and first run)
```
  terminal command          # about the step and required actions
  __________________________#____________________________________
                            # 0. Go to project's directory
  cp .env.sample .env       # 1. Configure environment
  bash project build        # 2. Prepare containers
  bash project first_start  # 3. Prepare DB (migrations) and start containers
                            # 4. Insert known e-pharmacies shop_ids at endpoint /api/v1/z_epharmacies/new?shop_ids=X (comma-separated)
  bash project stop         # 5. [Optional] Stop containers
```

### Usual run
```
  terminal command          # about the step and required actions
  __________________________#____________________________________
                            # 0. Go to project's directory
  bash project start        # 1. Start/restart containers
  bash project stop         # 2. Stop containers
```

### Endpoints, query parameters and JSON

1. The first endpoint for human operators (redirects to the basket_items page with appropriate params)

  [http://localhost:8080/api/v1/a_retailer_basket_items_yesterday](http://localhost:8080/api/v1/a_retailer_basket_items_yesterday)

2. The second most important endpoint is e-pharmacies adder (X has to be replaced with a list of comma separated `shop_id`s)

  [http://localhost:8080/api/v1/z_epharmacies/new?shop_ids=X](http://localhost:8080/api/v1/z_epharmacies/new?shop_ids=X)

3. Statistics information and debugging endpoints (consider it a help dashboard)

  [http://localhost:8080](http://localhost:8080)

#### Request

  [http://localhost:8080/api/v1/basket_items?view=flat&date=yesterday&page=1&per_page=25](http://localhost:8080/api/v1/basket_items?view=flat&date=yesterday&page=1&per_page=25)

 * `http://localhost:8080` - APP_PROTO, APP_HOST and APP_PORT from .env
 * `/api/v1/` - this part simply explains us the API is RESTful
 * `basket_items` - main endpoint for human operators
 * `?view=flat` - [!] this view is specifically designed with easy navigation in mind (also it is most suitable for table rendering), it includes links for setting `invalid_data` to true and false, setting :shop_id is also possible via additional link
 * `&date=yesterday` - obviously, that parameter is used if we want to select exact date (for instance, `2021-07-15`) but simply supplying "yesterday" is interpreted as yesterday's date in yyyy-mm-dd format
 * `&page=1` - page number in human-readable format: positive integer, from 1 (defaults to 1)
 * `&per_page=25` - maximum amount of results per page (defaults to 1)

Special parameters' available values:
```
  view: %w[basic flat full] # not every controller has all the views (basic is default),
  page: 1+ (values of unexisting pages will result in empty page status 200 return),
  per_page: 1+ (theoretically can be eny positive integer, should be restricted in production to avoid denial of service)
```

[!] There are two additional parameters: `invalid_data` (main purpose of this task) and `shop_id` (used for retailer A's internal system) - both can be used as filters.

#### Response
```
{
  "basket_items": [...],  # main data element
  "pagination": {...},    # information about amount of items totally and at the current page + links to navigate different pages
  "view": "flat",         # current view
  "page": 1,              # current page number
  "per_page": 25          # maximum results per page
}
```

### Additional console commands for debugging
1. Manually start nightly task (without that nightly task is scheduled with cron at 1am)
```
  bash project console api_shell
    bundle exec rake scheduled:whack_a_mole
```

2. Add basket items to target date (DATE), target retailer (RETAILER_ID) and in target amount (TOTAL)
```
  bash project console api_shell
    bundle exec rake random_data:make_basket_items DATE=yesterday RETAILER_ID=100 SKU=sd/fsd/f TOTAL=5
```

3. Add baskets
```
  bash project console api_shell
    bundle exec rake random_data:make_baskets
```

3. Add products
```
  bash project console api_shell
    bundle exec rake random_data:make_products
```

4. Add retailers
```
  bash project console api_shell
    bundle exec rake random_data:make_retailers
```

[!] Every of the abovementioned rake tasks supports ENV variables (can be passed _after_ command). Setting these results in related record creation if needed. `TOTAL` variable defaults to 1. Variables: `DATE, BASKET_ID, PRODUCT_ID, RETAILER_ID, SKU, TOTAL`.

* View list of available routes
```
  bash project rails routes | head -n 18
```

### Code quality
To ensure safety and beauty of code I used these tools:

* brakeman
* fasterer
* reek
* rubocop

### Remarks
* This should run on any podman host regardless of selinux state. I tested it on two machines:
```
 distro         | selinux   | podman    | podman-compose  | with sudo
 _______________|___________|___________|_________________|__________
 1. Arch Linux  | disabled  | 4.0.0dev  | 0.1.7dev        | yes
 2. Fedora 35   | enabled   | 3.4.0     | 0.1.7dev        | yes
```
* The naming convention behind prefixing auxiliary tables and project directories with `z_` is simply to put them last when sorted alphabetically.
* Integer/serial ID primary keys are prone to bruteforce and should be replaced with UUIDs. But as the task explicitly says _Retailer A_ has id of `100`, I am not switching pkey to UUIDs.
* This is a demonstrative project. For the production environment at least SSL encryption has to be enabled, some endpoints should be hidden, other - protected by authorisation.
