ARG RUBY_VERSION=3.0.2
FROM ruby:${RUBY_VERSION}-alpine

ARG TZ=Europe/Athens
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo ${TZ} > /etc/timezone \
    && apk add -u \
      --no-cache openssl \
      --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main \
    && apk add --update busybox-suid tzdata \
    && apk add --no-cache git openssh-client ca-certificates build-base postgresql-dev postgresql-client \
    && mkdir /app
WORKDIR /app

ARG BUNDLER_VERSION=2.2.28
ARG Z_GID=0
ARG Z_GROUP=root
ARG Z_UID=0
ARG Z_USER=root

ENV PATH /app/.local/share/gem/ruby/3.0.0/bin:/home/$Z_USER/.local/share/gem/ruby/3.0.0/bin:$PATH
RUN addgroup --gid ${Z_GID} ${Z_GROUP} || true \
    && adduser -u ${Z_UID} -G ${Z_GROUP} ${Z_USER} --disabled-password || true \
    && chown ${Z_USER}:${Z_GROUP} /app

USER ${Z_USER}
RUN gem install \
  bundler \
  --force \
  --version ${BUNDLER_VERSION}
