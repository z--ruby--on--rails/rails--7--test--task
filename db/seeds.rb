# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

unless Retailer.exists?(0)
  Retailer.create(
    id: 0,
    name: 'Sample retailer',
    url: 'https://example.com/'
  )
end

unless Product.exists?(0)
  Product.create(
    id: 0,
    name: 'Sample product',
    retailer_id: 0,
    sku: 'sp/0/0/0',
    price: 1
  )
end

unless Basket.exists?(0)
  Basket.create(
    id: 0,
    retailer_id: 0,
    date: Date.yesterday
  )
end

unless BasketItem.exists?(0)
  BasketItem.create(
    id: 0,
    basket_id: 0,
    product_id: 0,
    quantity: 1,
    revenue: 1
  )
end

unless ZEpharmacy.exists?(0)
  ZEpharmacy.create(
    id: 0,
    shop_id: 0
  )
end

# Retailer A

unless Retailer.exists?(100)
  Retailer.create(
    id: 100,
    name: 'Retailer A',
    url: 'https://retailer.a/'
  )
end
