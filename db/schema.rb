# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_07_085327) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "basket_items", id: :serial, force: :cascade do |t|
    t.integer "basket_id"
    t.integer "product_id"
    t.integer "quantity"
    t.decimal "revenue"
    t.boolean "invalid_data", default: false
    t.bigint "shop_id"
  end

  create_table "baskets", id: :serial, force: :cascade do |t|
    t.integer "retailer_id"
    t.date "date"
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "retailer_id"
    t.string "sku"
    t.decimal "price"
    t.index ["retailer_id", "sku"], name: "index_products_on_retailer_id_and_sku", unique: true
  end

  create_table "retailers", id: :serial, force: :cascade do |t|
    t.string "url"
    t.string "name"
    t.index ["name"], name: "index_retailers_on_name", unique: true
  end

  create_table "z_epharmacies", id: :serial, force: :cascade do |t|
    t.bigint "shop_id", null: false
    t.index ["shop_id"], name: "index_z_epharmacies_on_shop_id", unique: true
  end

  create_table "z_sku_in_shops", id: :serial, force: :cascade do |t|
    t.string "sku", null: false
    t.bigint "shop_id", null: false
    t.index ["shop_id", "sku"], name: "index_z_sku_in_shops_on_shop_id_and_sku", unique: true
  end

end
