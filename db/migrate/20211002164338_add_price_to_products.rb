# frozen_string_literal: true

class AddPriceToProducts < ActiveRecord::Migration[7.0]
  def up
    add_column :products, :price, :decimal
  end

  def down
    remove_column :products, :price
  end
end
