# frozen_string_literal: true

class AddShopIdToBasketItems < ActiveRecord::Migration[7.0]
  def up
    add_column :basket_items, :shop_id, :integer, limit: 8, default: nil
  end

  def down
    remove_column :basket_items, :shop_id
  end
end
