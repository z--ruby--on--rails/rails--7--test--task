# frozen_string_literal: true

class AddAutoIncrementEverywhere < ActiveRecord::Migration[7.0]
  def up
    %i[basket_items baskets products retailers].each do |table_name|
      execute "
        CREATE SEQUENCE #{table_name}_id_seq;
        ALTER TABLE ONLY #{table_name}
          ALTER COLUMN id SET DEFAULT nextval('#{table_name}_id_seq');
        ALTER SEQUENCE #{table_name}_id_seq OWNED BY #{table_name}.id;
        SELECT setval('#{table_name}_id_seq',
          COALESCE(max(id), 1)) FROM #{table_name};
      "
    end
  end

  def down
    %i[basket_items baskets products retailers].each do |table_name|
      execute "
        ALTER TABLE ONLY #{table_name}
          ALTER COLUMN id SET DEFAULT NULL;
        DROP SEQUENCE #{table_name}_id_seq;
      "
    end
  end
end
