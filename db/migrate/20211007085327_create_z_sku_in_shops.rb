# frozen_string_literal: true

class CreateZSkuInShops < ActiveRecord::Migration[7.0]
  def up
    create_table :z_sku_in_shops, id: :serial do |t|
      t.string :sku, null: false
      t.integer :shop_id, null: false, limit: 8
      t.index %i[shop_id sku], unique: true
    end
  end

  def down
    drop_table :z_sku_in_shops
  end
end
