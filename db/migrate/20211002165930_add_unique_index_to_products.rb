# frozen_string_literal: true

class AddUniqueIndexToProducts < ActiveRecord::Migration[7.0]
  def up
    add_index :products, %i[retailer_id sku], unique: true
  end

  def down
    remove_index :products, %i[retailer_id sku]
  end
end
