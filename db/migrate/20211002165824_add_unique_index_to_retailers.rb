# frozen_string_literal: true

class AddUniqueIndexToRetailers < ActiveRecord::Migration[7.0]
  def up
    add_index :retailers, :name, unique: true
  end

  def down
    remove_index :retailers, :name
  end
end
