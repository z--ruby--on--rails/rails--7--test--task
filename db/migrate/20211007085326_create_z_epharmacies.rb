# frozen_string_literal: true

class CreateZEpharmacies < ActiveRecord::Migration[7.0]
  def up
    create_table :z_epharmacies, id: :serial do |t|
      t.integer :shop_id, null: false, limit: 8, index: { unique: true }
    end
  end

  def down
    drop_table :z_epharmacies
  end
end
