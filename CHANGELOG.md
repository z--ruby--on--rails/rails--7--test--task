# CHANGELOG

## 2021-10-08
- typos fixed
- README is improved

## 2021-10-07

### Added
- "whitelist" table storing known e-pharmacies (`shop_id`s): `z_epharmacies`
- endpoints to manage whitelist

### Changed
- readme updated

### Fixed
- rake task for "random" data

## 2021-10-06

### Added
- date filter
- flat serialisers
- endpoints for human editors + links at full and flat views
- readme updated

## 2021-10-05

### Changed
- moved and united similar controller methods
- added advanced serialisers (`?view=full`) which show relations

### Added
- added httparty gem (https://github.com/jnunemaker/httparty)
- started adding service to fetch data from retailer A

## 2021-10-04

### Added
- added confirmed flag to basket_items table (indicates record's processed state)
- applied code quality assurance tools (brakeman, fasterer, reek, rubocop, rubocop-performance, rubocop-rails)

### Changed
- individual sequence for each table
- changed serialiser to Jat (https://github.com/aglushkov/jat)
- rescheduled :whack_a_mole task to 1am

### Fixed
- added :z to volume to avoid errors on hosts with selinux enabled
- sane serialisation with pagination (simple API)

## 2021-10-03

### Added
- added first run command (does initial DB preparation)
- added Alpine Linux dependency for crontab to Dockerfile (busybox-suid)
- implemented several debugging endpoints (only dev environment)
- added pagination with Pagy (https://github.com/ddnexus/pagy)
- started adding controllers

### Removed
- irrelevant README info

## 2021-10-02

### Added
- correct connection to DB, dump restoration at setup
- timezone is synced across containers (+ added as default to Rails app)
- state change monitor included
- generated schema with `bundle exec rake db:schema:dump` and created models
- created migration to auto-increment ID
- created migrations for unique cases and added price to products
- added "special" retailer to seeds
- created rake tasks to add random data (from faker gem)
- created schedule for nightly task with `bundle exec wheneverize .`

## 2021-10-01

### Added
- created the surface for future creation
- solved issue with access rights (by default container makes files as root)
- started creating DB
