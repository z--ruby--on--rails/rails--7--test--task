# frozen_string_literal: true

class RetailerADataParser
  include HTTParty

  def self.save_non_epharmacy_mappings(sku = 'xxx')
    res = get("https://example.api.that.is.not.real?sku=#{sku}")
    JSON.parse(res.body, symbolize_names: true)[:products].each do |product|
      product_shop_id = product[:shop_id]
      ZSkuInShop.find_or_create_by(sku: sku, shop_id: product_shop_id) unless ZEpharmacy.exists?(product_shop_id)
    end
  end
end
