# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :retailer
  has_many :basket_items, dependent: :destroy
end
