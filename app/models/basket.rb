# frozen_string_literal: true

class Basket < ApplicationRecord
  belongs_to :retailer
  has_many :basket_items, dependent: :destroy
end
