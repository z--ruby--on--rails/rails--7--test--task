# frozen_string_literal: true

class Retailer < ApplicationRecord
  has_many :baskets, dependent: :destroy
  has_many :products, dependent: :destroy
end
