# frozen_string_literal: true

module UrlHelper
  class << self
    def domain
      duo = Rails.application.config.action_controller.default_url_options
      port = ->(prt) { prt.present? ? ":#{prt}" : '' }.call(duo[:port])
      "#{duo[:protocol]}://#{duo[:host]}#{port}"
    end
  end
end
