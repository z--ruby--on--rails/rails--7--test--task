# frozen_string_literal: true

# :reek:TooManyStatements
module BasketItemsHelper
  def extract_date(date, fallback = nil)
    return unless date

    require 'date'
    yesterday = Time.zone.yesterday
    return yesterday if date == 'yesterday'

    begin
      Date.strptime(date, '%Y-%m-%d')
    rescue Date::Error
      fallback
    end
  end

  def get_bool(val)
    return unless val

    ActiveRecord::Type::Boolean.new.deserialize(val)
  end

  class << self
    def links(id)
      {
        mark_invalid: the_link(id, 'mark_invalid'),
        mark_valid: the_link(id, 'mark_valid'),
        mark_not_confirmed: the_link(id, 'mark_not_confirmed')
      }
    end

    def the_link(id, action)
      "#{UrlHelper.domain}/#{controller_path}/#{id}/#{action}"
    end
  end
end
