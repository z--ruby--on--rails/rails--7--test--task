# frozen_string_literal: true

class BasketBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :date
  attribute :retailer_id
end
