# frozen_string_literal: true

class BasketItemFullSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :quantity
  attribute :revenue

  attribute :basket, serializer: -> { BasketFullSerializer }
  attribute :product, serializer: -> { ProductFullSerializer }

  attribute :invalid_data
  attribute(:known_epharmacy) { ZEpharmacy.exists?(shop_id: object.shop_id) }

  attribute(:links) do
    Api::V1::BasketItemsController.links(object.id)
  end
end
