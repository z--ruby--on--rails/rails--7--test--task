# frozen_string_literal: true

class ProductBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :name
  attribute :sku
  attribute :price
  attribute :retailer_id
end
