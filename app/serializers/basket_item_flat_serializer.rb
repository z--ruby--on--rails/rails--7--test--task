# frozen_string_literal: true

class BasketItemFlatSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :quantity
  attribute :revenue

  attribute :basket_id
  attribute(:basket_date) { object.basket.date }
  attribute(:retailer_id) { object.basket.retailer.id }
  attribute(:retailer_name) { object.basket.retailer.name }
  attribute(:retailer_url) { object.basket.retailer.url }

  attribute :product_id
  attribute(:product_name) { object.product.name }
  attribute(:product_sku) { object.product.sku }
  attribute(:product_price) { object.product.price }

  attribute :invalid_data
  attribute(:known_epharmacy) { ZEpharmacy.exists?(shop_id: object.shop_id) }

  attribute(:links) do
    Api::V1::BasketItemsController.links(object.id)
  end
end
