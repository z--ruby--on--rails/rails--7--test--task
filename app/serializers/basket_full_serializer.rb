# frozen_string_literal: true

class BasketFullSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :date
  attribute :retailer, serializer: -> { RetailerBasicSerializer }
end
