# frozen_string_literal: true

class ZEpharmacyBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :shop_id
end
