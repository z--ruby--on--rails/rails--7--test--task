# frozen_string_literal: true

class ProductFullSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :name
  attribute :sku
  attribute :price

  attribute :retailer, serializer: -> { RetailerBasicSerializer }
end
