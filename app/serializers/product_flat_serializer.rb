# frozen_string_literal: true

class ProductFlatSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :name
  attribute :sku
  attribute :price

  attribute :retailer_id
  attribute(:retailer_name) { object.retailer.name }
  attribute(:retailer_url) { object.retailer.url }
end
