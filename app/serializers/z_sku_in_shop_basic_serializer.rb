# frozen_string_literal: true

class ZSkuInShopBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :sku
  attribute :shop_id
end
