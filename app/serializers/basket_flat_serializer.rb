# frozen_string_literal: true

class BasketFlatSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :date
  attribute :retailer, serializer: -> { RetailerBasicSerializer }
end
