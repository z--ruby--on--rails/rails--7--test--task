# frozen_string_literal: true

class BasketItemBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :quantity
  attribute :revenue

  attribute :basket_id
  attribute :product_id

  attribute :invalid_data
  attribute :shop_id
end
