# frozen_string_literal: true

class RetailerBasicSerializer < JatSerializer
  config[:exposed] = :all

  attribute :id
  attribute :name
  attribute :url
end
