# frozen_string_literal: true

class AboutController < ApplicationController
  include UrlHelper

  def index
    return unless Rails.env.development?

    render json: {
      main_endpoints: {
        "yesterday's basket items of retailer A": api_v1_a_retailer_basket_items_yesterday_index_url,
        'set target shop_ids as e-pharmacies': "#{api_v1_z_epharmacies_url}/new?shop_ids=X"
      },
      debug_endpoints: debug_endpoints,
      stats: stats
    }
  end

  private

  # rubocop:disable Metrics/MethodLength
  def debug_endpoints
    {
      basket_items: api_v1_basket_items_url(per_page: 25, view: 'flat'),
      last_basket_item: api_v1_basket_item_url(BasketItem.last || 0, view: 'full'),

      baskets: api_v1_baskets_url(per_page: 25, view: 'flat'),
      last_basket: api_v1_basket_url(Basket.last || 0, view: 'full'),

      products: api_v1_products_url(per_page: 25, view: 'flat'),
      last_product: api_v1_product_url(Product.last || 0, view: 'full'),

      retailers: api_v1_retailers_url(per_page: 25, view: 'flat'),
      last_retailer: api_v1_retailer_url(Retailer.last || 0, view: 'full'),

      'e-pharmacies': api_v1_z_epharmacies_url(per_page: 25),
      'sku in shops': api_v1_z_sku_in_shops_url(per_page: 25)
    }
  end
  # rubocop:enable Metrics/MethodLength

  # :reek:UtilityFunction
  def stats
    {
      basket_items: BasketItem.count,
      basket: Basket.count,
      products: Product.count,
      retailers: Retailer.count,
      'e-pharmacies': ZEpharmacy.count,
      'sku in shops': ZSkuInShop.count
    }
  end
end
