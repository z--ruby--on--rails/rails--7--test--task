# frozen_string_literal: true

class ApplicationController < ActionController::API
  # skip_before_filter :verify_authenticity_token, if: :json_request?
  include Pagy::Backend

  # def json_request?
  #   request.format.json?
  # end

  rescue_from ActiveRecord::RecordNotFound, with: :not_found_error
  rescue_from Pagy::OverflowError, with: :not_found_error

  private

  def destroy
    # TODO

    head :ok
  end

  def not_found_error
    # TODO

    head :ok
  end
end
