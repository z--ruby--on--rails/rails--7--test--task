# frozen_string_literal: true

module Api
  module V1
    class ApplicationController < ::Api::ApplicationController
      include UrlHelper

      def index
        params_positive(:page, :per_page)
        pagy, data = pagy(default_relation, items: params[:per_page])

        render json: {
          "#{the_root_name}": api_v1_serialize(the_view, data),
          pagination: data.present? ? pagy_metadata(pagy, absolute: true) : nil
        }.merge(meta).compact
      end

      def show
        render json: {
          "#{controller_name.singularize}": api_v1_serialize(the_view,
                                                             the_model_name.find(
                                                               params[:id]
                                                             )),
          view: the_view
        }
      end

      private

      def meta
        {
          view: the_view,
          page: params[:page],
          per_page: params[:per_page]
        }
      end

      def params_positive(*keys)
        keys.each do |key|
          params[key] = [params[key].to_i, 1].max
        end
      end

      def params_whitelist
        %i[page per_page view]
      end

      def the_model_name
        controller_name.classify.constantize
      end

      def the_root_name
        controller_name
      end

      def the_view
        @the_view ||= ->(ds, pv) { ds.include?(pv.presence) ? pv : ds[0] }.call(default_serializers, params[:view])
      end

      def api_v1_serialize(view, data)
        "::#{
          controller_name.classify
        }#{
          view.titleize
        }Serializer".constantize.to_h(data)
      end

      def default_relation
        the_model_name.order(id: :asc)
      end

      def default_serializers
        %w[basic flat full]
      end
    end
  end
end
