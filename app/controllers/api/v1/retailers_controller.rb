# frozen_string_literal: true

module Api
  module V1
    class RetailersController < ::Api::V1::ApplicationController
      private

      def default_serializers
        %w[basic]
      end
    end
  end
end
