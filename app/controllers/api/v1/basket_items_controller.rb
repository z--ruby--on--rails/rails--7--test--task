# frozen_string_literal: true

module Api
  module V1
    class BasketItemsController < ::Api::V1::ApplicationController
      include BasketItemsHelper

      def set_invalid
        set_data(
          {
            invalid_data: true
          },
          'basket item was set INVALID successfully',
          'basket item\'s invalidation failed'
        )
      end

      def set_approved
        set_data(
          {
            invalid_data: false
          },
          'basket item was set APPROVED successfully',
          'basket item\'s approval failed'
        )
      end

      def set_shop_id
        params_id = params[:shop_id]
        if params_id == 'X'
          render json: {
            code: 1,
            message: 'Please, replace X in the URL with shop_id'
          }
        else
          set_data({ shop_id: params_id.to_i }, 'Chosen shop_id was set', 'Setting shop_id failed')
        end
      end

      private

      class << self
        def links(id)
          {
            set_invalid: local_link_to(id, 'set_invalid'),
            set_approved: local_link_to(id, 'set_approved'),
            set_shop_id: "#{local_link_to(id, 'set_shop_id')}?shop_id=X"
          }
        end

        def local_link_to(id, action)
          "#{UrlHelper.domain}/#{controller_path}/#{id}/#{action}"
        end
      end

      def set_data(data, message_success, message_failure)
        code, message = if BasketItem.find(params[:id]).update(data)
                          [0, message_success]
                        else
                          [1, message_failure]
                        end

        render json: {
          code: code,
          message: message
        }
      end

      def default_relation
        basket_items = if the_filters.present?
                         ::BasketItem.where(the_filters).order(id: :asc)
                       else
                         ::BasketItem.order(id: :asc)
                       end

        if the_filters_basket.present?
          basket_items.joins(:basket).where(basket: the_filters_basket)
        else
          basket_items
        end
      end

      def the_date
        @the_date ||= extract_date(params[:date].presence, Time.zone.yesterday)
      end

      def the_filters
        @the_filters ||= {
          confirmed: get_bool(params[:confirmed].presence),
          invalid_data: get_bool(params[:invalid_data].presence)
        }.compact_blank.presence
      end

      def the_filters_basket
        @the_filters_basket ||= {
          date: the_date,
          retailer_id: params[:retailer_id].presence
        }.compact_blank.presence
      end
    end
  end
end
