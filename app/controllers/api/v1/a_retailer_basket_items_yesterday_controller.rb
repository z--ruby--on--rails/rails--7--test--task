# frozen_string_literal: true

module Api
  module V1
    class ARetailerBasketItemsYesterdayController < ::Api::V1::ApplicationController
      def index
        redirect_to api_v1_basket_items_url(
          view: 'flat',
          date: 'yesterday',
          retailer_id: 100,
          per_page: 25
        ), status: :permanent_redirect
      end
    end
  end
end
