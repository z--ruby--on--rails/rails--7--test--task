# frozen_string_literal: true

module Api
  module V1
    class ZEpharmaciesController < ::Api::V1::ApplicationController
      def new
        if params_shop_id == 'X'
          render_resp(1, 'Please, replace X in the URL with comma-separated list of shop ids to create')
        else
          params_shop_id.split(',').each do |shop_id|
            ZEpharmacy.find_or_create_by(shop_id: shop_id.to_i)
          end
          render_resp(0, 'Chosen shop_ids were added to e-pharmacies')
        end
      end

      def edit
        if params_id == 'X'
          render_resp(1, 'Please, replace X in the URL with comma-separated list of shop ids to remove')
        else
          params_id.split(',').each do |shop_id|
            filter = { shop_id: shop_id.to_i }
            ZEpharmacy.destroy_by(filter) if ZEpharmacy.exists?(filter)
          end
          render_resp(0, 'Chosen shop_ids were removed from e-pharmacies')
        end
      end

      private

      def params_id
        @params_id ||= params[:id]
      end

      def params_shop_id
        @params_shop_id ||= params[:shop_ids]
      end

      def render_resp(code, message)
        render json: {
          code: code,
          message: message
        }
      end

      def default_relation
        ZEpharmacy.all
      end

      def the_root_name
        'e-pharmacies'
      end

      def message_error; end
    end
  end
end
