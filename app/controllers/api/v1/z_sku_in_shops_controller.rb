# frozen_string_literal: true

module Api
  module V1
    class ZSkuInShopsController < ::Api::V1::ApplicationController
      private

      def default_relation
        ZSkuInShop.all
      end

      def the_root_name
        'sku in shops'
      end
    end
  end
end
