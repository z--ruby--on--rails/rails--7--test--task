# frozen_string_literal: true

namespace :scheduled do
  require_relative '../../app/helpers/basket_items_helper'
  include BasketItemsHelper

  desc 'extract and invalidate basket items'
  task whack_a_mole: :environment do
    filters = {
      date: extract_date(ENV['DATE']) || Time.zone.yesterday,
      retailer_id: ENV['RETAILER_A_ID'].presence&.to_i || 100
    }
    BasketItem.joins(:basket)
              .where(basket: filters)
              .each do |basket_item|
      RetailerADataParser.save_non_epharmacy_mappings(basket_item.product.sku)
    end

    ZSkuInShop.select(:sku).distinct.map(&:sku).each do |sku|
      product = Product.find_by(sku: sku, retailer_id: filters[:retailer_id])
      next unless product

      shop_id = ZSkuInShop.find_by(sku: sku).shop_id
      product.basket_items.joins(:basket).where(basket: { date: filters[:date] })
             .update(invalid_data: true, shop_id: shop_id)
    end
  end
end
