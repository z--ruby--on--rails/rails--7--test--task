# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace :random_data do
  # https://github.com/faker-ruby/faker

  def totally_positive_amount(env_total = nil)
    [env_total.presence.to_i, 1].max
  end

  def the_date(env_date = nil)
    extract_date(env_date) || Faker::Date.between(from: 2.days.ago, to: Time.zone.today)
  end

  def the_retailer(env_retailer_id = nil)
    env_retailer_id.present? ? Retailer.find(env_retailer_id) : Retailer.all.sample
  end

  def the_basket(env_date = nil, env_retailer_id = nil, env_basket_id = nil)
    filters = {
      date: env_date.presence,
      retailer_id: env_retailer_id.present? ? env_retailer_id.to_i : nil
    }.compact
    filtered_random_basket = Basket.find_by(filters) unless filters.empty?
    return filtered_random_basket if filtered_random_basket.present?
    return if filters.present? && filtered_random_basket.blank?

    env_basket_id.present? ? Basket.find(env_basket_id) : Basket.all.sample
  end

  def the_product(env_sku = nil, env_retailer_id = nil, env_product_id = nil)
    filters = {
      sku: env_sku.presence,
      retailer_id: env_retailer_id.present? ? env_retailer_id.to_i : nil
    }.compact
    filtered_random_product = Product.find_by(filters) unless filters.empty?
    return filtered_random_product if filtered_random_product.present?
    return if filters.present? && filtered_random_product.blank?

    env_product_id.present? ? Product.find(env_product_id) : Product.all.sample
  end

  def the_sku(env_sku = nil)
    return env_sku if env_sku.present?

    size = Faker::Number.between(from: 2, to: 4)
    item_name = [Faker::Alphanumeric.alpha(number: size)]

    Faker::Number.between(from: 1, to: 3).times do
      item_name.append(Faker::Number.number(digits: size))
    end
    item_name.join(
      ['/', ':', '-'].sample
    )
  end

  ###

  desc 'add new basket items'
  task make_basket_items: :environment do
    n = totally_positive_amount(ENV['TOTAL'])

    product = the_product(ENV['SKU'], ENV['RETAILER_ID'],
                          ENV['PRODUCT_ID'])
    if product.blank?
      Rake::Task['random_data:make_products'].invoke
      product = Product.last
    end

    basket = the_basket(ENV['DATE'], ENV['RETAILER_ID'],
                        ENV['BASKET_ID'])
    if basket.blank?
      Rake::Task['random_data:make_baskets'].invoke
      basket = Basket.last
    end

    n.times do
      amount = Faker::Number.number(digits: 1)
      profit = product.price * amount

      BasketItem.create(
        basket_id: basket.id,
        product_id: product.id,
        quantity: amount,
        revenue: profit
      )
    end
  end

  desc 'add new baskets'
  task make_baskets: :environment do
    n = totally_positive_amount(ENV['TOTAL'])

    retailer = the_retailer(ENV['RETAILER_ID'])
    if retailer.blank?
      Rake::Task['random_data:make_retailers'].invoke
      retailer = Retailer.last
    end

    n.times do
      Basket.create(
        retailer_id: retailer.id,
        date: the_date(ENV['DATE'])
      )
    end
  end

  desc 'add new products'
  task make_products: :environment do
    n = totally_positive_amount(ENV['TOTAL'])

    retailer = the_retailer(ENV['RETAILER_ID'])
    if retailer.blank?
      Rake::Task['random_data:make_retailers'].invoke
      retailer = Retailer.last
    end

    n.times do
      Product.create(
        name: Faker::Commerce.unique.product_name,
        retailer_id: retailer.id,
        sku: the_sku(ENV['SKU']),
        price: Faker::Commerce.price
      )
    end
  end

  desc 'add new retailers'
  task make_retailers: :environment do
    totally_positive_amount(ENV['TOTAL']).times do
      Retailer.create(
        name: Faker::Company.unique.name,
        url: Faker::Internet.unique.url
      )
    end
  end

  desc 'add new e-pharmacies'
  task make_z_epharmacies: :environment do
    totally_positive_amount(ENV['TOTAL']).times do
      ZEpharmacy.create(
        shop_id: Faker::Number.number
      )
    end
  end
end
# rubocop:enable Metrics/BlockLength
