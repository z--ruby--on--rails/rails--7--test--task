#!/bin/bash

. .env

export PGPASSWORD="$APP_DB_PASSWORD"
$CONTAINER_ENGINE exec -it \
  ${APP_DB_BASENAME}_db_1 psql \
  -U $APP_DB_USERNAME \
  -d ${APP_DB_BASENAME}_dev
