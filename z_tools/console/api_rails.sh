#!/bin/bash

. .env

$CONTAINER_ENGINE exec -it \
  ${APP_DB_BASENAME}_api_1 sh -c "\
    bundle exec rails ${1:-c} \
  "
