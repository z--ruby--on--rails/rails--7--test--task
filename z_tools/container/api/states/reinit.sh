#!/bin/sh

echo 'reinitialising' \
  > /app/z_tools/.state.lock
rm /app/Gemfile* \
  2> /dev/null

gem install --user-install \
  rails \
  --force \
  --version ${RAILS_VERSION}

bundle init
rails new . \
  --api \
  --database=postgresql \
  --skip-git \
  --force
bundle binstubs bundler
bundle install \
  --local \
  --jobs 4 \
  --retry 4
