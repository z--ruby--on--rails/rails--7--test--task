#!/bin/sh

echo 'running' \
  > /app/z_tools/.state.lock
rm -f tmp/pids/server.pid \
  && bundle exec rails s \
    -p ${APP_PORT} \
    -b '0.0.0.0'
