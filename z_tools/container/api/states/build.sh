#!/bin/sh

echo 'building' \
  > /app/z_tools/.state.lock
bundle
bundle install \
  --local \
  --jobs 4 \
  --retry 4

bundle exec whenever \
  --update-crontab

echo
echo '--- READY! ---'
echo

rm /app/z_tools/.state.lock
