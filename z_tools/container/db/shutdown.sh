#!/bin/sh

kill -INT `head -1 /usr/local/pgsql/data/postmaster.pid` \
  > /dev/null
# this pid file could be missing, probably I should do something about it
