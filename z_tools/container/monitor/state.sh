#!/bin/sh

FILENAME="/app/z_tools/.state.lock"
STATE=$(cat "${FILENAME}" 2> /dev/null || "")

while [ "$STATE" != "" ]; do
  # echo "[INFO] $(date -Is) | STATE: ${STATE}"
  STATE_new=$(cat "${FILENAME}" 2> /dev/null)

  if [ "${STATE_new}" == "" ]; then
    # echo 'State file is missing. Stopping...'
    ::
  elif [ "${STATE_new}" != "${STATE}" ]; then
    echo "[INFO] $(date -Is) | STATE changed: ${STATE_new}"
    echo
  fi
  STATE="${STATE_new}"
  sleep 5
done

# exit
echo "[INFO] $(date -Is) | DONE (Press Ctrl+C to exit if process is still active)"
