#!/bin/bash

cd $C_DIR
. .env

rm -f ./z_tools/.state.lock \
  2> /dev/null
$CONTAINER_COMPOSER -p $APP_DB_BASENAME down

$CONTAINER_COMPOSER -p $APP_DB_BASENAME build \
  --build-arg BUNDLER_VERSION=$BUNDLER_VERSION \
  --build-arg RUBY_VERSION=$RUBY_VERSION \
  --build-arg TZ=$TZ \
  --build-arg Z_GID=$Z_GID \
  --build-arg Z_GROUP=$Z_GROUP \
  --build-arg Z_UID=$Z_UID \
  --build-arg Z_USER=$Z_USER

echo "build" \
  > ./z_tools/.state.lock
$CONTAINER_COMPOSER -p $APP_DB_BASENAME up \
  2> /dev/null

# TODO: stop DB if it was initialised previously and was not shutdown this time

rm -f ./z_tools/.state.lock
