#!/bin/bash

cd $C_DIR
. .env

rm -f ./z_tools/.state.lock \
  2> /dev/null
$CONTAINER_COMPOSER -p $APP_DB_BASENAME down

echo "start" \
  > ./z_tools/.state.lock
$CONTAINER_COMPOSER -p $APP_DB_BASENAME up \
  2> /dev/null

rm -f ./z_tools/.state.lock
