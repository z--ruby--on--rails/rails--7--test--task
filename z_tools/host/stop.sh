#!/bin/bash

cd $C_DIR
. .env

rm -f ./tmp/command 2> /dev/null
$CONTAINER_COMPOSER -p $APP_DB_BASENAME down
