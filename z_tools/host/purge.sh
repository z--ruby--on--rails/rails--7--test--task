#!/bin/bash

cd $C_DIR
. .env

rm -f ./z_tools/.state.lock \
  2> /dev/null
$CONTAINER_COMPOSER -p $APP_DB_BASENAME down

$CONTAINER_ENGINE container rm \
  ${APP_DB_BASENAME}_api_1 \
  ${APP_DB_BASENAME}_db_1 \
  2> /dev/null
  # ${APP_DB_BASENAME}_monitor_1

$CONTAINER_ENGINE volume rm \
  ${APP_DB_BASENAME}_bundle_cache \
  ${APP_DB_BASENAME}_database \
  2> /dev/null
